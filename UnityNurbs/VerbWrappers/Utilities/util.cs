﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityNurbs.Utilities
{
    static internal class ExtensionMethods
    {


        public static HaxeArray<T> ToHaxeArray<T>(this T[] obj)
        {
            if (obj == null)
                return null;

            var array = new HaxeArray<T>();

            for (int i = 0; i < obj.Length; i++)
            {
                array.push(obj[i]);
            }

            return array;
        }

        public static T[] ToArray<T>(this HaxeArray<T> array)
        {
            //pop gotta be in reverse!
            
            var returnArray = new T[array.length];

            for (int i = array.length-1; i >= 0; i--)
            {
                returnArray[i] = array.pop().value;
            }

            return returnArray;
        }

        //This is probably broken! Might need additional testing if giving weird results
        public static HaxeArray<object> ToHaxeMatrix(this Matrix4x4 matrix)
        {
            var output = new HaxeArray<object>();

            for (int i = 0; i < 4; i++)
            {
                var row = matrix.GetRow(0);
                var array = new HaxeArray<double>();
                array.push(row.w);
                array.push(row.x);
                array.push(row.y);
                array.push(row.z);

                output.push((object)array);
            }

            return output;

        }

        public static HaxeArray<double> ToHaxeDoubleArray(this Vector3 vector)
        {
            var returnStruct = new HaxeArray<double>();
            returnStruct.push(vector.x);
            returnStruct.push(vector.y);
            returnStruct.push(vector.z);

            return returnStruct;
        }

        public static HaxeArray<double> ToHaxeDoubleArray(this Vector2 vector)
        {
            var returnStruct = new HaxeArray<double>();
            returnStruct.push(vector.x);
            returnStruct.push(vector.y);

            return returnStruct;
        }

        public static HaxeArray<object> ToHaxeArray<T>(this T[,] obj)
        {
            if (obj == null)
                return null;

            var outerArray = new HaxeArray<object>();

            for (int u = 0; u < obj.GetLength(0); u++)
            {
                var array = new HaxeArray<T>();

                for (int v = 0; v < obj.GetLength(1); v++)
                {
                    array.push(obj[u, v]);
                }

                outerArray.push(array);
            }

            return outerArray;
        }

        public static HaxeArray<object> ToHaxeObjectArray(this Vector3[,] obj)
        {
            if (obj == null)
                return null;

            var outerArray = new HaxeArray<object>();

            for (int u = 0; u < obj.GetLength(0); u++)
            {
                var array = new HaxeArray<object>();

                for (int v = 0; v < obj.GetLength(1); v++)
                {
                    array.push(obj[u, v].ToHaxeDoubleArray());
                }

                outerArray.push(array);
            }

            return outerArray;
        }

        public static HaxeArray<object> ToHaxeObjectArray(this Vector3[] vectorArray)
        {
            var returnStruct = new HaxeArray<object>();

            for (int i = 0; i < vectorArray.Length; i++)
            {
                returnStruct.push(vectorArray[i].ToHaxeDoubleArray());
            }
            return returnStruct;
        }

        public static HaxeArray<object> ToHaxeObjectArray(this Vector2[] vectorArray)
        {
            var returnStruct = new HaxeArray<object>();

            for (int i = 0; i < vectorArray.Length; i++)
            {
                returnStruct.push(vectorArray[i].ToHaxeDoubleArray());
            }
            return returnStruct;
        }

        public static HaxeArray<object> ToHaxeObjectArray(this NurbsCurve[] nurbsCurveArray)
        {
            var returnStruct = new HaxeArray<object>();

            for (int i = 0; i < nurbsCurveArray.Length; i++)
            {
                returnStruct.push(nurbsCurveArray[i].GetVerb());
            }
            return returnStruct;
        }

        public static Vector3 ToVector3(this HaxeArray<double> array)
        {
            //Pop gotta be in reverse!

            var z = (float)array.pop().value;
            var y = (float)array.pop().value;
            var x = (float)array.pop().value;

            return new Vector3(x, y, z);
        }

        public static Vector2 ToVector2(this HaxeArray<double> array)
        {
            //Pop gotta be in reverse!

            var y = (float)array.pop().value;
            var x = (float)array.pop().value;

            return new Vector2(x, y);
        }

        public static Vector3[] ToVector3Array(this HaxeArray<object> array)
        {
            var returnArray = new Vector3[array.length];

            for (int i = array.length - 1; i >= 0; i--)
            {
                var haxeVector = array.pop().value as HaxeArray<double>;
                var vector = haxeVector.ToVector3();
                returnArray[i] = vector;
            }

            return returnArray;
        }

        public static Vector2[] ToVector2Array(this HaxeArray<object> array)
        {
            var returnArray = new Vector2[array.length];

            for (int i = array.length - 1; i >= 0; i--)
            {
                var haxeVector = array.pop().value as HaxeArray<double>;
                var vector = haxeVector.ToVector2();
                returnArray[i] = vector;
            }

            return returnArray;
        }

        public static int[] ToIntArray(this HaxeArray<object> array)
        {
            var returnArray = new int[array.length*3];

            for (int i = array.length - 1; i >= 0; i--)
            {
                var triangle = array.pop().value as HaxeArray<int>;

                for (int u = 0; u < 3; u++)
                {
                    var index = triangle[u];
                    returnArray[i] = index;
                }
            }

            return returnArray;
        }

        public static CurveLengthSample ToUnity(this verb.eval.CurveLengthSample sample)
        {
            return new CurveLengthSample((float)sample.u, (float)sample.len);
        }
    }
}
