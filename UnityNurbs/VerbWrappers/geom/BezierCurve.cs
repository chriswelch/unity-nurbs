﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    /// <summary>
    /// A Bezier curve is a common spline curve
    /// </summary>
    public class BezierCurve : NurbsCurve
    {
        /// <summary>
        /// Create a bezier curve
        /// </summary>
        /// <param name="points">Array of control points</param>
        /// <param name="weights">Array of control point weights</param>
        public BezierCurve(Vector3[] points, double[] weights)
        {
            _nurbs = new verb.geom.BezierCurve(points.ToHaxeObjectArray(), weights.ToHaxeArray());
        }
    }
}
