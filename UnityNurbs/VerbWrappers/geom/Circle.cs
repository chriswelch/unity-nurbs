﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    /// <summary>
    /// A Circle is a three dimensional curve representing the points that are equidistant from a point in a particular plane
    /// </summary>
    public class Circle : Arc
    {
        //constructors


        /// <summary>
        /// Create a circle
        /// </summary>
        /// <param name="center">Vector3 representing the center of the circle</param>
        /// <param name="xAxis">Vector3 representing the xaxis</param>
        /// <param name="yAxis">Vector3 representing the perpendicular yaxis</param>
        /// <param name="radius">Radius of the circle</param>
        public Circle(Vector3 center, Vector3 xAxis, Vector3 yAxis, float radius)
        {
            _nurbs = new verb.geom.Circle(ExtensionMethods.ToHaxeDoubleArray(center), ExtensionMethods.ToHaxeDoubleArray(xAxis), ExtensionMethods.ToHaxeDoubleArray(yAxis), radius);
        }
    }
}
