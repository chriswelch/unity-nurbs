﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    /// <summary>
    /// An Arc is a three dimensional curve representing a subset of a full Circle
    /// </summary>
    public class Arc : NurbsCurve
    {
        //parameters

        /// <summary>
        /// Vector3 representing the center of the circle
        /// </summary>
        public Vector3 Center { get { return ((verb.geom.Arc)_nurbs).center().ToVector3(); } }

        /// <summary>
        /// Vector3 representing the xaxis
        /// </summary>
        public Vector3 XAxis { get { return ((verb.geom.Arc)_nurbs).xaxis().ToVector3(); } }

        /// <summary>
        /// Vector3 representing the perpendicular yaxis
        /// </summary>
        public Vector3 YAxis { get { return ((verb.geom.Arc)_nurbs).yaxis().ToVector3(); } }

        /// <summary>
        /// Radius of the arc
        /// </summary>
        public float Radius { get { return (float)((verb.geom.Arc)_nurbs).radius(); } }

        /// <summary>
        /// Start angle in radians
        /// </summary>
        public float MinAngle { get { return (float)((verb.geom.Arc)_nurbs).minAngle(); } }

        /// <summary>
        /// End angle in radians
        /// </summary>
        public float MaxAngle { get { return (float)((verb.geom.Arc)_nurbs).maxAngle(); } }

        //constructors

        /// <summary>
        /// For inheritence purposes only
        /// </summary>
        protected Arc() { }

        /// <summary>
        /// Create an Arc
        /// </summary>
        /// <param name="center">Vector3 representing the center of the circle</param>
        /// <param name="xAxis">Vector3 representing the xaxis</param>
        /// <param name="yAxis">Vector3 representing the perpendicular yaxis</param>
        /// <param name="radius">Radius of the arc</param>
        /// <param name="startAngle">Start angle of arc</param>
        /// <param name="endAngle">End angle of arc</param>
        public Arc(Vector3 center, Vector3 xAxis, Vector3 yAxis, float radius, float startAngle, float endAngle)
        {
            _nurbs = new verb.geom.Arc(ExtensionMethods.ToHaxeDoubleArray(center), ExtensionMethods.ToHaxeDoubleArray(xAxis), ExtensionMethods.ToHaxeDoubleArray(yAxis), radius, startAngle, endAngle);
        }

    }
}
