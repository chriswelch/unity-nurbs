﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    public class Line : NurbsCurve
    {
        //parameters

        public Vector3 StartPoint { get { return _line.start().ToVector3(); } }
        public Vector3 EndPoint { get { return _line.end().ToVector3(); } }

        private verb.geom.Line _line;

        //constructors

        public Line(Vector3 startPoint, Vector3 endPoint)
        {
            _line = new verb.geom.Line(startPoint.ToHaxeDoubleArray(), endPoint.ToHaxeDoubleArray());
            _nurbs = _line;
        }
    }
}
