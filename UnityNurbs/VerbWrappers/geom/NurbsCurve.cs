﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    public class NurbsCurve : ICloneable
    {
        //Properties (all readonly)

        /// <summary>
        /// The degree of the curve
        /// </summary>
        public int Degree { get { return _nurbs.degree(); } }

        /// <summary>
        /// The knot array
        /// </summary>
        public double[] Knots { get { return _nurbs.knots().ToArray(); } }
        //public Vector3[] ControlPoints { get { return _nurbs.controlPoints().To} }

        /// <summary>
        /// Array of weight values
        /// </summary>
        public double[] Weights { get { return _nurbs.weights().ToArray(); } }

        /// <summary>
        /// A parameter value representing the start point of the domain of the curve
        /// </summary>
        public float DomainStart { get { return (float)_nurbs.domain().min; } }

        /// <summary>
        /// An parameter value representing the end point of the domain of the curve
        /// </summary>
        public float DomainEnd { get { return (float)_nurbs.domain().max; } }

        /// <summary>
        /// Internal verb nurbs curve
        /// </summary>
        protected verb.geom.NurbsCurve _nurbs = new verb.geom.NurbsCurve(new haxe.lang.EmptyObject());

        //Constructors

        /// <summary>
        /// For inheritence purposes only
        /// </summary>
        protected NurbsCurve()
        {
        }

        // Currently not using nurbsdata
        //public NurbsCurve(NurbsCurveData data)
        //{
        //    var _nurbs = new verb.geom.NurbsCurve(data._nurbsData);
        //}

        /// <summary>
        /// Construct a NurbsCurve by degree, knots, control points, weights
        /// </summary>
        /// <param name="degree">The degree</param>
        /// <param name="knots">The knot array</param>
        /// <param name="controlPoints">Array of control points</param>
        /// <param name="weights">Array of weight values</param>
        public NurbsCurve(int degree, double[] knots, Vector3[] controlPoints, double[] weights)
        {
            _nurbs = verb.geom.NurbsCurve.byKnotsControlPointsWeights(degree, knots.ToHaxeArray(), controlPoints.ToHaxeObjectArray(), weights.ToHaxeArray());
        }

        /// <summary>
        /// Construct a NurbsCurve by interpolating a collection of points. The resultant curve will pass through all of the points.
        /// </summary>
        /// <param name="points">An array of points</param>
        /// <param name="degree">Optional : The degree of resultant curve</param>
        public NurbsCurve(Vector3[] points, int degree = 3)
        {
            _nurbs = verb.geom.NurbsCurve.byPoints(points.ToHaxeObjectArray(), degree);
        }

        //Methods

        //AsNurbs()

        public object Clone()
        {
            return new NurbsCurve() { _nurbs = GetVerb() };
        }


        /// <summary>
        /// Transform a curve with the given matrix
        /// </summary>
        /// <param name="matrix">Matrix representing the transformation</param>
        /// <returns>The new nurbs curve </returns>
        public NurbsCurve Transform(Matrix4x4 matrix)
        {
            var transformedCurve = _nurbs.transform(matrix.ToHaxeMatrix());
            return new NurbsCurve()
            {
                _nurbs = _nurbs
            };
        }

        /// <summary>
        /// Sample a point at the given parameter
        /// </summary>
        /// <param name="t">The parameter to sample the curve</param>
        /// <returns>The point at the parameter</returns>
        public Vector3 PointAtParameter(float t)
        {
            var point = _nurbs.point(t);
            var vector = point.ToVector3();
            return vector;
        }

        /// <summary>
        /// Obtain the curve tangent at the given parameter. This is the first derivative and is not normalized
        /// </summary>
        /// <param name="t">The parameter to sample the curve</param>
        /// <returns>A vector representing the tangent at the given parameter</returns>
        public Vector3 TangentAtParameter(float t)
        {
            return _nurbs.tangent(t).ToVector3();
        }

        /// <summary>
        /// Get derivatives at a given parameter
        /// </summary>
        /// <param name="t">The parameter to sample the curve</param>
        /// <param name="derivativeCount">The number of derivatives to obtain</param>
        /// <returns>An array of vectors</returns>
        public Vector3[] DerivativesAtParameter(float t, int derivativeCount)
        {
            return _nurbs.derivatives(t, derivativeCount).ToVector3Array();
        }

        /// <summary>
        /// Determine the closest point on the curve to the given point
        /// </summary>
        /// <param name="point">The point for testing</param>
        /// <returns>The closest point on the curve</returns>
        public Vector3 ClosestPoint(Vector3 point)
        {
            return _nurbs.closestPoint(point.ToHaxeDoubleArray()).ToVector3();
        }

        /// <summary>
        /// Determine the closest parameter on the curve to the given point
        /// </summary>
        /// <param name="point">The point for testing</param>
        /// <returns>The closest parameter on the curve</returns>
        public float ClosestParameter(Vector3 point)
        {
            return (float)_nurbs.closestParam(point.ToHaxeDoubleArray());
        }

        /// <summary>
        /// Determine the arc length of the curve
        /// </summary>
        /// <returns>The length of the curve</returns>
        public float Length()
        {
            return (float)_nurbs.length();
        }

        /// <summary>
        /// Determine the arc length of the curve at the given parameter
        /// </summary>
        /// <param name="t">The parameter at which to evaluate</param>
        /// <returns>The length of the curve at the given parameter</returns>
        public float LengthAtParameter(float t)
        {
            return (float)_nurbs.lengthAtParam(t);
        }

        /// <summary>
        /// Determine the parameter of the curve at the given arc length
        /// </summary>
        /// <param name="length">The arc length at which to determine the parameter</param>
        /// <returns>The length of the curve at the given parameter</returns>
        public float ParameterAtLength(float length)
        {
            return (float)_nurbs.paramAtLength(length, verb.core.Constants.TOLERANCE);
        }

        /// <summary>
        /// Determine the parameters necessary to divide the curve into equal arc length segments
        /// </summary>
        /// <param name="divisions">Number of divisions of the curve</param>
        /// <returns>Length and parameter of each sample</returns>
        public CurveLengthSample[] CurveLengthSampleAtEqualArcLength(int divisions)
        {
            var samples = _nurbs.divideByEqualArcLength(divisions);

            var output = new CurveLengthSample[samples.length];

            for (int i = samples.length--; i >= 0; i++)
            {
                var curveSample = samples.pop().value as verb.eval.CurveLengthSample;
                output[i] = curveSample.ToUnity();
            }
            return output;
        }

        public Vector3[] PointAtEqualArcLength(int divisions)
        {
            var samples = _nurbs.divideByEqualArcLength(divisions);

            var output = new Vector3[samples.length];

            for (int i = samples.length--; i >= 0; i++)
            {
                var curveSample = samples.pop().value as verb.eval.CurveLengthSample;

                
                output[i] = _nurbs.point(curveSample.ToUnity().u).ToVector3();
            }
            return output;
        }

        /// <summary>
        /// Given the distance to divide the curve, determine the parameters necessary to divide the curve into equal arc length segments
        /// </summary>
        /// <param name="arcLength">Arc length of each segment</param>
        /// <returns>Length and parameter of each sample</returns>
        public CurveLengthSample[] DivideByArcLength(float arcLength)
        {
            var samples = _nurbs.divideByArcLength(arcLength);

            var output = new CurveLengthSample[samples.length];

            for (int i = samples.length--; i >= 0; i++)
            {
                var curveSample = samples.pop().value as verb.eval.CurveLengthSample;
                output[i] = curveSample.ToUnity();
            }
            return output;
        }

        /// <summary>
        /// Split the curve at the given parameter
        /// </summary>
        /// <param name="t">The parameter at which to split the curve</param>
        /// <returns>Two curves - one at the lower end of the parameter range and one at the higher end.</returns>
        public NurbsCurve[] SplitAtParameter(float t)
        {
            //pop in reverse!

            var split = _nurbs.split(t).Array_cast<verb.geom.NurbsCurve>() as HaxeArray<verb.geom.NurbsCurve>;
            var b = split.pop().value;
            var a = split.pop().value;

            return new NurbsCurve[]
            {
                new NurbsCurve(){_nurbs = a},
                new NurbsCurve(){_nurbs = b}
            };
        }

        /// <summary>
        /// Reverse the parameterization of the curve
        /// </summary>
        /// <returns>A reversed curve</returns>
        public NurbsCurve Reverse()
        {
            return new NurbsCurve()
            {
                _nurbs = _nurbs.reverse()
            };
        }

        /// <summary>
        /// Tessellate a curve at a given tolerance
        /// </summary>
        /// <param name="tolerance">The tolerance at which to sample the curve</param>
        /// <returns>A point represented as an array</returns>
        public Vector3[] Tessellate(float tolerance = 0.01f)
        {
            var points = _nurbs.tessellate(tolerance);
            return points.ToVector3Array();
        }

        //Custom Functions

        /// <summary>
        /// Draw a debug curve in the editor window in current matrix
        /// </summary>
        /// <param name="color">Color of the curve</param>
        /// <param name="duration">Optional : If larger than zero, will display over multiple frames</param>
        /// <param name="tolerance">Optional: Amount of tessalation applied to the curve to obtain a desired accuracy</param>
        public void DebugDraw(Color color, float duration = 0f, float tolerance = 0.01f)
        {
            var points = Tessellate(tolerance);

            for (int i = 0; i < points.Length - 1; i++)
            {
                if (duration == 0f)
                {
                    Debug.DrawLine(points[i], points[i + 1], color);
                }
                else
                {
                    Debug.DrawLine(points[i], points[i + 1], color, duration);
                }
            }
        }

        /// <summary>
        /// raw a debug curve in the editor window in current matrix
        /// </summary>
        /// <param name="gradient">A gradient to apply across the current curve. Ignores domain</param>
        /// <param name="duration">Optional : If larger than zero, will display over multiple frames</param>
        /// <param name="tolerance">Optional: Amount of tessalation applied to the curve to obtain a desired accuracy</param>
        public void DebugDraw(Gradient gradient, float duration = 0f, float tolerance = 0.01f)
        {
            var points = Tessellate(tolerance);

            var increment = 1f / (points.Length - 1);

            for (int i = 0; i < points.Length - 1; i++)
            {
                if (duration == 0f)
                {
                    Debug.DrawLine(points[i], points[i + 1], gradient.Evaluate(i * increment));
                }
                else
                {
                    Debug.DrawLine(points[i], points[i + 1], gradient.Evaluate(i * increment), duration);
                }
            }
        }

        /// <summary>
        /// Returns a clone of the current verb nurbs curve
        /// </summary>
        /// <returns>A cloned nurbs curve</returns>
        public verb.geom.NurbsCurve GetVerb()
        {
            return _nurbs.clone();
        }

        public NurbsCurveData AddToGameObject(GameObject go)
        {
            var curveData = go.AddComponent<NurbsCurveData>();
            curveData.Curve = this;

            var typeString = GetType().ToString(); 
            curveData.Type = typeString.Split('.')[1];

            return curveData;
        }

        public GameObject ToGameObject()
        {
            var obj = new GameObject();

            var points = Tessellate();

            var lineRenderer = obj.AddComponent<LineRenderer>();
            lineRenderer.positionCount = points.Length;
            lineRenderer.SetPositions(points);

            var data = AddToGameObject(obj);
            obj.name = data.Type;

            return obj;
        }

        public GameObject ToGameObject(GameObject prefab)
        {
            var obj = GameObject.Instantiate(prefab);

            var points = Tessellate();

            var lineRenderer = obj.GetComponent<LineRenderer>();
            lineRenderer.positionCount = points.Length;
            lineRenderer.SetPositions(points);

            var data = AddToGameObject(obj);
            obj.name = data.Type;

            return obj;
        }


    }

    // public class NurbsCurveData
    // {
    //     internal verb.core.NurbsCurveData _nurbsData;
    // 
    //     public NurbsCurveData(int degree, double[] knots, Vector3[] controlPoints )
    //     {
    //         _nurbsData = new verb.core.NurbsCurveData(degree, knots.ToVerbArray(), controlPoints.ToVerbArray());
    //     }
    // }

    /// <summary>
    /// Output of a curve sample
    /// </summary>
    public struct CurveLengthSample
    {
        /// <summary>
        /// Parameter on curve of sample
        /// </summary>
        public float u;
        /// <summary>
        /// Distance along curve of sample
        /// </summary>
        public float length;

        /// <summary>
        /// Evaluation of a point on a curve
        /// </summary>
        /// <param name="u">parameter of point on curve</param>
        /// <param name="length">Length along the curve of the sample</param>
        public CurveLengthSample(float u, float length)
        {
            this.u = u;
            this.length = length;
        }
    }
}