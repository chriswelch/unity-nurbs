﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    /// <summary>
    /// An EllipseArc is a subset of an Ellipse
    /// </summary>
    public class EllipseArc : NurbsCurve
    {
        /// <summary>
        /// Vector3 representing the center of the arc
        /// </summary>
        public Vector3 Center { get { return ((verb.geom.EllipseArc)_nurbs).center().ToVector3(); } }

        /// <summary>
        /// Vector3 representing the xaxis
        /// </summary>
        public Vector3 XAxis { get { return ((verb.geom.EllipseArc)_nurbs).xaxis().ToVector3(); } }

        /// <summary>
        /// Vector3 representing the perpendicular yaxis
        /// </summary>
        public Vector3 YAxis { get { return ((verb.geom.EllipseArc)_nurbs).yaxis().ToVector3(); } }

        /// <summary>
        /// Start angle in radians
        /// </summary>
        public float MinAngle { get { return (float)((verb.geom.EllipseArc)_nurbs).minAngle(); } }

        /// <summary>
        /// End angle in radians
        /// </summary>
        public float MaxAngle { get { return (float)((verb.geom.EllipseArc)_nurbs).maxAngle(); } }

        //constructors

        /// <summary>
        /// For inheritence purposes only
        /// </summary>
        protected EllipseArc() { }

        /// <summary>
        /// Create an EllipseArc
        /// </summary>
        /// <param name="center">Vector3 representing the center of the circle</param>
        /// <param name="xAxis">Vector3 representing the xaxis</param>
        /// <param name="yAxis">Vector3 representing the perpendicular yaxis</param>
        /// <param name="startAngle">Start angle of arc</param>
        /// <param name="endAngle">End angle of arc</param>
        public EllipseArc(Vector3 center, Vector3 xAxis, Vector3 yAxis, float startAngle, float endAngle)
        {
            _nurbs = new verb.geom.EllipseArc(ExtensionMethods.ToHaxeDoubleArray(center), ExtensionMethods.ToHaxeDoubleArray(xAxis), ExtensionMethods.ToHaxeDoubleArray(yAxis), startAngle, endAngle);
        }
    }
}
