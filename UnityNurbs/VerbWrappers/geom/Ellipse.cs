﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    /// <summary>
    /// An EllipseArc is a subset of an Ellipse
    /// </summary>
    public class Ellipse : EllipseArc
    {
        //constructors

        /// <summary>
        /// For inheritence purposes only
        /// </summary>
        protected Ellipse() { }

        /// <summary>
        /// Create an Ellipse
        /// </summary>
        /// <param name="center">Vector3 representing the center of the ellipse</param>
        /// <param name="xAxis">Vector3 representing the xaxis</param>
        /// <param name="yAxis">Vector3 representing the perpendicular yaxis</param>
        public Ellipse(Vector3 center, Vector3 xAxis, Vector3 yAxis)
        {
            _nurbs = new verb.geom.Ellipse(ExtensionMethods.ToHaxeDoubleArray(center), ExtensionMethods.ToHaxeDoubleArray(xAxis), ExtensionMethods.ToHaxeDoubleArray(yAxis));
        }
    }
}
