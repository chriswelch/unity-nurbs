﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityNurbs.Utilities;

namespace UnityNurbs
{
    public class NurbsSurface
    {
        private verb.geom.NurbsSurface _nurbs = new verb.geom.NurbsSurface(new haxe.lang.EmptyObject());

        //Properties

        /// <summary>
        /// The degree in the U direction
        /// </summary>
        public int DegreeU { get { return _nurbs.degreeU(); } }

        /// <summary>
        /// The degree in the V direction
        /// </summary>
        public int DegreeV { get { return _nurbs.degreeV(); } }


        /// <summary>
        /// The knot array in the U direction
        /// </summary>
        public double[] KnotsU { get { return _nurbs.knotsU().ToArray(); } }

        /// <summary>
        /// The knot array in the V direction
        /// </summary>
        public double[] KnotsV { get { return _nurbs.knotsV().ToArray(); } }

        //Constructors

        /// <summary>
        /// For inheritence purposes only
        /// </summary>
        protected NurbsSurface()
        {
        }

        public NurbsSurface(int degreeU, int degreeV, double[] knotsU, double[] knotsV, Vector3[,] controlPoints, double[,] weights )
        {
            _nurbs = verb.geom.NurbsSurface.byKnotsControlPointsWeights(degreeU, degreeV, knotsU.ToHaxeArray(), knotsV.ToHaxeArray(), controlPoints.ToHaxeObjectArray(), weights.ToHaxeArray());
        }

        /// <summary>
        /// Construct a NurbsSurface from four perimeter points in counter-clockwise order
        /// </summary>
        /// <param name="p0">The first point</param>
        /// <param name="p1">The second point</param>
        /// <param name="p2">The third point</param>
        /// <param name="p3">The fourth point</param>
        public NurbsSurface(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            _nurbs = verb.geom.NurbsSurface.byCorners(p0.ToHaxeDoubleArray(), p1.ToHaxeDoubleArray(), p2.ToHaxeDoubleArray(), p3.ToHaxeDoubleArray());
        }

        /// <summary>
        /// Construct a NurbsSurface by lofting between a collection of curves
        /// </summary>
        /// <param name="curves">A collection of curves</param>
        /// <param name="degreeV">The curve degree</param>
        public NurbsSurface(NurbsCurve[] curves,int degreeV)
        {
            _nurbs = verb.geom.NurbsSurface.byLoftingCurves(curves.ToHaxeObjectArray(),degreeV);
        }

        /// <summary>
        /// Obtain a point on the surface at the given parameter
        /// </summary>
        /// <param name="u">The u parameter</param>
        /// <param name="v">The v parameter</param>
        /// <returns>A point on the surface</returns>
        public Vector3 Point(float u, float v)
        {
            return _nurbs.point((double)u, (double)v).ToVector3();
        }

        /// <summary>
        /// Obtain the normal to the surface at the given parameter
        /// </summary>
        /// <param name="u">The u parameter</param>
        /// <param name="v">The v parameter</param>
        /// <returns></returns>
        public Vector3 Normal(Vector2 uv)
        {
            return _nurbs.normal((double)uv.x, (double)uv.y).ToVector3();
        }

        /// <summary>
        /// Get the closest parameter on the surface to a point
        /// </summary>
        /// <param name="point">The point</param>
        /// <returns>The closest uv parameter</returns>
        public Vector2 ClosestParam(Vector3 point)
        {
            return _nurbs.closestParam(point.ToHaxeDoubleArray()).ToVector2();
        }

        /// <summary>
        /// Get the closest point on the surface to a point
        /// </summary>
        /// <param name="point">The point</param>
        /// <returns>The closest point</returns>
        public Vector3 ClosestPoint(Vector3 point)
        {
            return _nurbs.closestPoint(point.ToHaxeDoubleArray()).ToVector3();
        }

        /// <summary>
        /// Split a surface along either the U or V parameter
        /// </summary>
        /// <param name="u">The parameter to do the split</param>
        /// <param name="toggleV">Whether to divide in V or U</param>
        /// <returns>A length 2 array with two new NurbsSurface objects</returns>
        public NurbsSurface[] Split(float u, bool toggleV)
        {
            return _nurbs.split((double)u, toggleV).ToArray<object>().Select(x => new NurbsSurface() { _nurbs = (verb.geom.NurbsSurface)x }).ToArray();
        }

        public Mesh Tessellate()
        {
            var options = new verb.eval.AdaptiveRefinementOptions();
            var meshData = _nurbs.tessellate(options);

            var points = meshData.points.ToVector3Array();
            var faces = meshData.faces.ToIntArray();
            var normals = meshData.normals.ToVector3Array();
            var uvs = meshData.uvs.ToVector2Array();

            var mesh = new Mesh();
            mesh.vertices = points;
            mesh.triangles = faces;
            mesh.normals = normals;
            mesh.uv = uvs;

            mesh.RecalculateBounds();

            return mesh;
        }

        ///


    }
}
