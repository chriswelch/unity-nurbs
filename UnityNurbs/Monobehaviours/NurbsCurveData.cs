﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NurbsCurveData : MonoBehaviour {

    public string Type;
    public UnityNurbs.NurbsCurve Curve;
}
