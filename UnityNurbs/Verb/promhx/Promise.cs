// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise<T> : global::promhx.@base.AsyncBase<T>, global::promhx.Promise {
		
		public Promise(global::haxe.lang.EmptyObject empty) : base(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) )) {
		}
		
		
		public Promise(global::promhx.Deferred<T> d) : base(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) )) {
			global::promhx.Promise<object>.__hx_ctor_promhx_Promise<T>(((global::promhx.Promise<T>) (this) ), ((global::promhx.Deferred<T>) (d) ));
		}
		
		
		public static void __hx_ctor_promhx_Promise<T_c>(global::promhx.Promise<T_c> __hx_this, global::promhx.Deferred<T_c> d) {
			global::promhx.@base.AsyncBase<object>.__hx_ctor_promhx_base_AsyncBase<T_c>(((global::promhx.@base.AsyncBase<T_c>) (__hx_this) ), ((global::promhx.Deferred<T_c>) (d) ));
			__hx_this._rejected = false;
		}
		
		
		public static new object __hx_cast<T_c_c>(global::promhx.Promise me) {
			return ( (( me != null )) ? (me.promhx_Promise_cast<T_c_c>()) : default(object) );
		}
		
		
		public static global::promhx.Promise<object> whenAll<T1>(object itb) {
			global::promhx.Promise<object> ret = new global::promhx.Promise<object>(default(global::promhx.Deferred<object>));
			{
				object all = itb;
				global::promhx.@base.AsyncBase<object> next = ret;
				global::haxe.lang.Function cthen = new global::promhx.Promise_whenAll_86__Fun_0<T1>(next, all);
				{
					object a2 = ((object) (global::haxe.lang.Runtime.callField(all, "iterator", 328878574, null)) );
					while (global::haxe.lang.Runtime.toBool(global::haxe.lang.Runtime.callField(a2, "hasNext", 407283053, null))) {
						global::promhx.@base.AsyncBase<T1> a3 = ((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (global::haxe.lang.Runtime.callField(a2, "next", 1224901875, null)) ))) );
						global::HaxeArray<object> a4 = a3._update;
						global::haxe.lang.Function f = cthen;
						global::HaxeArray<object> _g1 = new global::HaxeArray<object>(new object[]{});
						{
							object a21 = ((object) (global::haxe.lang.Runtime.callField(all, "iterator", 328878574, null)) );
							while (global::haxe.lang.Runtime.toBool(global::haxe.lang.Runtime.callField(a21, "hasNext", 407283053, null))) {
								global::promhx.@base.AsyncBase<T1> a22 = ((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (global::haxe.lang.Runtime.callField(a21, "next", 1224901875, null)) ))) );
								if (( a22 != a3 )) {
									_g1.push(a22);
								}
								
							}
							
						}
						
						global::HaxeArray<object> a11 = _g1;
						global::promhx.@base.AsyncBase<T1> a23 = a3;
						global::haxe.lang.Function tmp = new global::promhx.Promise_whenAll_86__Fun<T1>(f, a23, a11);
						a4.push(new global::haxe.lang.DynamicObject(new int[]{641597244, 1963057964}, new object[]{((global::promhx.@base.AsyncBase) (next) ), tmp}, new int[]{}, new double[]{}));
					}
					
				}
				
				if (global::promhx.@base.AsyncBase<object>.allFulfilled(all)) {
					global::HaxeArray<T1> _g2 = new global::HaxeArray<T1>(new T1[]{});
					{
						object a5 = ((object) (global::haxe.lang.Runtime.callField(all, "iterator", 328878574, null)) );
						while (global::haxe.lang.Runtime.toBool(global::haxe.lang.Runtime.callField(a5, "hasNext", 407283053, null))) {
							global::promhx.@base.AsyncBase<T1> a6 = ((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (global::haxe.lang.Runtime.callField(a5, "next", 1224901875, null)) ))) );
							_g2.push(a6._val);
						}
						
					}
					
					next.handleResolve(_g2);
				}
				
			}
			
			return ret;
		}
		
		
		public static global::promhx.Promise<T2> promise<T2>(T2 _val) {
			global::promhx.Promise<T2> ret = new global::promhx.Promise<T2>(default(global::promhx.Deferred<T2>));
			ret.handleResolve(_val);
			return ret;
		}
		
		
		public virtual object promhx_Promise_cast<T_c>() {
			if (global::haxe.lang.Runtime.eq(typeof(T), typeof(T_c))) {
				return this;
			}
			
			global::promhx.Promise<T_c> new_me = new global::promhx.Promise<T_c>(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) ));
			global::HaxeArray<object> fields = global::HaxeReflect.fields(this);
			int i = 0;
			while (( i < fields.length )) {
				string field = global::haxe.lang.Runtime.toString(fields[i++]);
				global::HaxeReflect.setField(new_me, field, global::HaxeReflect.field(this, field));
			}
			
			return new_me;
		}
		
		
		public override object promhx_base_AsyncBase_cast<T_c>() {
			return this.promhx_Promise_cast<T_c>();
		}
		
		
		public bool _rejected;
		
		public bool isRejected() {
			return this._rejected;
		}
		
		
		public virtual void reject(object e) {
			this._rejected = true;
			this.handleError(e);
		}
		
		
		public override void handleResolve(T val) {
			if (this._resolved) {
				string msg = "Promise has already been resolved";
				throw global::haxe.lang.HaxeException.wrap(global::promhx.error.PromiseError.AlreadyResolved(msg));
			}
			
			this._resolve(val);
		}
		
		
		public override global::promhx.@base.AsyncBase<A> then<A>(global::haxe.lang.Function f) {
			global::promhx.Promise<A> ret = new global::promhx.Promise<A>(default(global::promhx.Deferred<A>));
			{
				global::promhx.@base.AsyncBase<A> next = ret;
				global::haxe.lang.Function f1 = f;
				object __temp_stmt2 = null;
				{
					global::haxe.lang.Function __temp_odecl1 = new global::promhx.Promise_then_106__Fun<A, T>(next, f1);
					__temp_stmt2 = new global::haxe.lang.DynamicObject(new int[]{641597244, 1963057964}, new object[]{((global::promhx.@base.AsyncBase) (next) ), __temp_odecl1}, new int[]{}, new double[]{});
				}
				
				this._update.push(__temp_stmt2);
				global::promhx.@base.AsyncBase<object>.immediateLinkUpdate<T, A>(((global::promhx.@base.AsyncBase<T>) (this) ), ((global::promhx.@base.AsyncBase<A>) (next) ), ((global::haxe.lang.Function) (f1) ));
			}
			
			return ret;
		}
		
		
		public override void unlink(global::promhx.@base.AsyncBase to) {
			global::promhx.Promise<T> _gthis = this;
			{
				global::promhx.@base.EventLoop.queue.@add(new global::promhx.Promise_unlink_111__Fun<T>(to, _gthis));
				global::promhx.@base.EventLoop.continueOnNextLoop();
			}
			
		}
		
		
		public override void handleError(object error) {
			this._rejected = true;
			this._handleError(error);
		}
		
		
		public virtual global::promhx.Promise<A> pipe<A>(global::haxe.lang.Function f) {
			global::promhx.Promise<A> ret = new global::promhx.Promise<A>(default(global::promhx.Deferred<A>));
			{
				global::promhx.@base.AsyncBase<A> ret1 = ret;
				global::haxe.lang.Function f1 = f;
				bool[] linked = new bool[]{false};
				global::haxe.lang.Function linkf = new global::promhx.Promise_pipe_129__Fun<A, T>(ret1, linked, f1);
				this._update.push(new global::haxe.lang.DynamicObject(new int[]{641597244, 1963057964}, new object[]{((global::promhx.@base.AsyncBase) (ret1) ), linkf}, new int[]{}, new double[]{}));
				if (( this._resolved &&  ! (this._pending)  )) {
					try {
						linkf.__hx_invoke1_o(default(double), this._val);
					}
					catch (global::System.Exception __temp_catchallException1){
						global::haxe.lang.Exceptions.exception = __temp_catchallException1;
						object __temp_catchall2 = __temp_catchallException1;
						if (( __temp_catchall2 is global::haxe.lang.HaxeException )) {
							__temp_catchall2 = ((global::haxe.lang.HaxeException) (__temp_catchallException1) ).obj;
						}
						
						{
							object e = __temp_catchall2;
							ret1.handleError(e);
						}
						
					}
					
					
				}
				
			}
			
			return ret;
		}
		
		
		public virtual global::promhx.Promise<T> errorPipe(global::haxe.lang.Function f) {
			global::promhx.Promise<T> ret = new global::promhx.Promise<T>(default(global::promhx.Deferred<T>));
			this.catchError(new global::promhx.Promise_errorPipe_138__Fun<T>(ret, f));
			global::promhx.Promise<object>.__hx_cast<object>(((global::promhx.Promise) (this.then<object>(((global::haxe.lang.Function) (new global::haxe.lang.Closure(ret, "_resolve", 555248749)) ))) ));
			return ret;
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1931081437:
					{
						this._rejected = global::haxe.lang.Runtime.toBool(@value);
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1891659798:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "errorPipe", 1891659798)) );
					}
					
					
					case 1247278126:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "pipe", 1247278126)) );
					}
					
					
					case 628324096:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "handleError", 628324096)) );
					}
					
					
					case 1703419603:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "unlink", 1703419603)) );
					}
					
					
					case 1291584221:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "then", 1291584221)) );
					}
					
					
					case 2144664612:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "handleResolve", 2144664612)) );
					}
					
					
					case 42291551:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "reject", 42291551)) );
					}
					
					
					case 640881032:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "isRejected", 640881032)) );
					}
					
					
					case 1931081437:
					{
						return this._rejected;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 2144664612:
					case 1291584221:
					case 1703419603:
					case 628324096:
					{
						return global::haxe.lang.Runtime.slowCallField(this, field, dynargs);
					}
					
					
					case 1891659798:
					{
						return this.errorPipe(((global::haxe.lang.Function) (dynargs[0]) ));
					}
					
					
					case 1247278126:
					{
						return this.pipe<object>(((global::haxe.lang.Function) (dynargs[0]) ));
					}
					
					
					case 42291551:
					{
						this.reject(dynargs[0]);
						break;
					}
					
					
					case 640881032:
					{
						return this.isRejected();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
				return null;
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("_rejected");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_then_106__Fun<A, T> : global::haxe.lang.Function {
		
		public Promise_then_106__Fun(global::promhx.@base.AsyncBase<A> next, global::haxe.lang.Function f1) : base(1, 0) {
			this.next = next;
			this.f1 = f1;
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			T x = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (global::haxe.lang.Runtime.genericCast<T>(((object) (__fn_float1) ))) : (global::haxe.lang.Runtime.genericCast<T>(__fn_dyn1)) );
			A tmp = global::haxe.lang.Runtime.genericCast<A>(this.f1.__hx_invoke1_o(default(double), x));
			this.next.handleResolve(tmp);
			return null;
		}
		
		
		public global::promhx.@base.AsyncBase<A> next;
		
		public global::haxe.lang.Function f1;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_unlink_117__Fun<T> : global::haxe.lang.Function {
		
		public Promise_unlink_117__Fun(global::promhx.@base.AsyncBase to) : base(1, 0) {
			this.to = to;
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			object x = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (((object) (__fn_float1) )) : (((object) (__fn_dyn1) )) );
			return ( ((global::promhx.@base.AsyncBase) (global::haxe.lang.Runtime.getField(x, "async", 641597244, true)) ) != this.to );
		}
		
		
		public global::promhx.@base.AsyncBase to;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_unlink_111__Fun<T> : global::haxe.lang.Function {
		
		public Promise_unlink_111__Fun(global::promhx.@base.AsyncBase to, global::promhx.Promise<T> _gthis) : base(0, 0) {
			this.to = to;
			this._gthis = _gthis;
		}
		
		
		public override object __hx_invoke0_o() {
			if ( ! (this._gthis._fulfilled) ) {
				string msg = "Downstream Promise is not fullfilled";
				this._gthis.handleError(global::promhx.error.PromiseError.DownstreamNotFullfilled(msg));
			}
			else {
				this._gthis._update = this._gthis._update.filter(new global::promhx.Promise_unlink_117__Fun<T>(this.to));
			}
			
			return null;
		}
		
		
		public global::promhx.@base.AsyncBase to;
		
		public global::promhx.Promise<T> _gthis;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_pipe_129__Fun_0<A> : global::haxe.lang.Function {
		
		public Promise_pipe_129__Fun_0() : base(1, 0) {
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			A x1 = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (global::haxe.lang.Runtime.genericCast<A>(((object) (__fn_float1) ))) : (global::haxe.lang.Runtime.genericCast<A>(__fn_dyn1)) );
			return x1;
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_pipe_129__Fun<A, T> : global::haxe.lang.Function {
		
		public Promise_pipe_129__Fun(global::promhx.@base.AsyncBase<A> ret1, bool[] linked, global::haxe.lang.Function f1) : base(1, 0) {
			this.ret1 = ret1;
			this.linked = linked;
			this.f1 = f1;
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			T x = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (global::haxe.lang.Runtime.genericCast<T>(((object) (__fn_float1) ))) : (global::haxe.lang.Runtime.genericCast<T>(__fn_dyn1)) );
			if ( ! (this.linked[0]) ) {
				this.linked[0] = true;
				global::promhx.@base.AsyncBase<A> pipe_ret = ((global::promhx.@base.AsyncBase<A>) (global::promhx.@base.AsyncBase<object>.__hx_cast<A>(((global::promhx.@base.AsyncBase) (this.f1.__hx_invoke1_o(default(double), x)) ))) );
				object __temp_stmt2 = null;
				{
					global::haxe.lang.Function __temp_odecl1 = ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this.ret1, "handleResolve", 2144664612)) );
					__temp_stmt2 = new global::haxe.lang.DynamicObject(new int[]{641597244, 1963057964}, new object[]{((global::promhx.@base.AsyncBase) (this.ret1) ), __temp_odecl1}, new int[]{}, new double[]{});
				}
				
				pipe_ret._update.push(__temp_stmt2);
				global::promhx.@base.AsyncBase<object>.immediateLinkUpdate<A, A>(((global::promhx.@base.AsyncBase<A>) (pipe_ret) ), ((global::promhx.@base.AsyncBase<A>) (this.ret1) ), ((global::haxe.lang.Function) (new global::promhx.Promise_pipe_129__Fun_0<A>()) ));
			}
			
			return null;
		}
		
		
		public global::promhx.@base.AsyncBase<A> ret1;
		
		public bool[] linked;
		
		public global::haxe.lang.Function f1;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_errorPipe_138__Fun<T> : global::haxe.lang.Function {
		
		public Promise_errorPipe_138__Fun(global::promhx.Promise<T> ret, global::haxe.lang.Function f) : base(1, 0) {
			this.ret = ret;
			this.f = f;
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			object e = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (((object) (__fn_float1) )) : (((object) (__fn_dyn1) )) );
			global::promhx.Promise<T> piped = ((global::promhx.Promise<T>) (global::promhx.Promise<object>.__hx_cast<T>(((global::promhx.Promise) (this.f.__hx_invoke1_o(default(double), e)) ))) );
			global::promhx.Promise<object>.__hx_cast<object>(((global::promhx.Promise) (piped.then<object>(((global::haxe.lang.Function) (new global::haxe.lang.Closure(this.ret, "_resolve", 555248749)) ))) ));
			return null;
		}
		
		
		public global::promhx.Promise<T> ret;
		
		public global::haxe.lang.Function f;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_whenAll_86__Fun_0<T1> : global::haxe.lang.Function {
		
		public Promise_whenAll_86__Fun_0(global::promhx.@base.AsyncBase<object> next, object all) : base(3, 0) {
			this.next = next;
			this.all = all;
		}
		
		
		public override object __hx_invoke3_o(double __fn_float1, object __fn_dyn1, double __fn_float2, object __fn_dyn2, double __fn_float3, object __fn_dyn3) {
			T1 v = ( (( __fn_dyn3 == global::haxe.lang.Runtime.undefined )) ? (global::haxe.lang.Runtime.genericCast<T1>(((object) (__fn_float3) ))) : (global::haxe.lang.Runtime.genericCast<T1>(__fn_dyn3)) );
			global::promhx.@base.AsyncBase<T1> current = ( (( __fn_dyn2 == global::haxe.lang.Runtime.undefined )) ? (((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (((object) (__fn_float2) )) ))) )) : (((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (__fn_dyn2) ))) )) );
			global::HaxeArray<object> arr = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (((global::HaxeArray<object>) (global::HaxeArray<object>.__hx_cast<object>(((global::Array) (((object) (__fn_float1) )) ))) )) : (((global::HaxeArray<object>) (global::HaxeArray<object>.__hx_cast<object>(((global::Array) (__fn_dyn1) ))) )) );
			if (( ( arr.length == 0 ) || global::promhx.@base.AsyncBase<object>.allFulfilled(arr) )) {
				global::HaxeArray<T1> _g = new global::HaxeArray<T1>(new T1[]{});
				{
					object a = ((object) (global::haxe.lang.Runtime.callField(this.all, "iterator", 328878574, null)) );
					while (global::haxe.lang.Runtime.toBool(global::haxe.lang.Runtime.callField(a, "hasNext", 407283053, null))) {
						global::promhx.@base.AsyncBase<T1> a1 = ((global::promhx.@base.AsyncBase<T1>) (global::promhx.@base.AsyncBase<object>.__hx_cast<T1>(((global::promhx.@base.AsyncBase) (global::haxe.lang.Runtime.callField(a, "next", 1224901875, null)) ))) );
						_g.push(( (( a1 == current )) ? (v) : (a1._val) ));
					}
					
				}
				
				global::HaxeArray<T1> vals = _g;
				this.next.handleResolve(vals);
			}
			
			return null;
		}
		
		
		public global::promhx.@base.AsyncBase<object> next;
		
		public object all;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	public class Promise_whenAll_86__Fun<T1> : global::haxe.lang.Function {
		
		public Promise_whenAll_86__Fun(global::haxe.lang.Function f, global::promhx.@base.AsyncBase<T1> a23, global::HaxeArray<object> a11) : base(1, 0) {
			this.f = f;
			this.a23 = a23;
			this.a11 = a11;
		}
		
		
		public override object __hx_invoke1_o(double __fn_float1, object __fn_dyn1) {
			T1 v1 = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (global::haxe.lang.Runtime.genericCast<T1>(((object) (__fn_float1) ))) : (global::haxe.lang.Runtime.genericCast<T1>(__fn_dyn1)) );
			this.f.__hx_invoke3_o(default(double), this.a11, default(double), this.a23, default(double), v1);
			return null;
		}
		
		
		public global::haxe.lang.Function f;
		
		public global::promhx.@base.AsyncBase<T1> a23;
		
		public global::HaxeArray<object> a11;
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace promhx {
	[global::haxe.lang.GenericInterface(typeof(global::promhx.Promise<object>))]
	public interface Promise : global::haxe.lang.IHxObject, global::promhx.@base.AsyncBase, global::haxe.lang.IGenericObject {
		
		object promhx_Promise_cast<T_c>();
		
		object promhx_base_AsyncBase_cast<T_c>();
		
		bool isRejected();
		
		void reject(object e);
		
	}
}


