// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public class Mat : global::haxe.lang.HxObject {
		
		public Mat(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public Mat() {
			global::verb.core.Mat.__hx_ctor_verb_core_Mat(this);
		}
		
		
		public static void __hx_ctor_verb_core_Mat(global::verb.core.Mat __hx_this) {
		}
		
		
		public static global::HaxeArray<object> mul(double a, global::HaxeArray<object> b) {
			global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
			{
				int _g2 = 0;
				int _g1 = b.length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					_g.push(global::verb.core.Vec.mul(a, ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (b[i]) ))) )));
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<object> mult(global::HaxeArray<object> x, global::HaxeArray<object> y) {
			unchecked {
				int p = default(int);
				int q = default(int);
				int r = default(int);
				global::HaxeArray<object> ret = null;
				global::HaxeArray<double> foo = null;
				global::HaxeArray<double> bar = null;
				double woo = default(double);
				int i0 = default(int);
				object k0 = null;
				object p0 = null;
				object r0 = null;
				p = x.length;
				q = y.length;
				r = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (y[0]) ))) ).length;
				ret = new global::HaxeArray<object>();
				int i = ( p - 1 );
				int j = 0;
				int k = 0;
				while (( i >= 0 )) {
					foo = new global::HaxeArray<double>();
					bar = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (x[i]) ))) );
					k = ( r - 1 );
					while (( k >= 0 )) {
						woo = ( bar[( q - 1 )] * ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (y[( q - 1 )]) ))) )[k] );
						j = ( q - 2 );
						while (( j >= 1 )) {
							i0 = ( j - 1 );
							woo += ( ( bar[j] * ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (y[j]) ))) )[k] ) + ( bar[i0] * ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (y[i0]) ))) )[k] ) );
							j -= 2;
						}
						
						if (( j == 0 )) {
							woo += ( bar[0] * ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (y[0]) ))) )[k] );
						}
						
						foo[k] = woo;
						 -- k;
					}
					
					ret[i] = foo;
					 -- i;
				}
				
				return ret;
			}
		}
		
		
		public static global::HaxeArray<object> @add(global::HaxeArray<object> a, global::HaxeArray<object> b) {
			global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
			{
				int _g2 = 0;
				int _g1 = a.length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					_g.push(global::verb.core.Vec.@add(((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (a[i]) ))) ), ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (b[i]) ))) )));
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<object> div(global::HaxeArray<object> a, double b) {
			global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
			{
				int _g2 = 0;
				int _g1 = a.length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					_g.push(global::verb.core.Vec.div(((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (a[i]) ))) ), b));
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<object> sub(global::HaxeArray<object> a, global::HaxeArray<object> b) {
			global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
			{
				int _g2 = 0;
				int _g1 = a.length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					_g.push(global::verb.core.Vec.sub(((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (a[i]) ))) ), ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (b[i]) ))) )));
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<double> dot(global::HaxeArray<object> a, global::HaxeArray<double> b) {
			global::HaxeArray<double> _g = new global::HaxeArray<double>(new double[]{});
			{
				int _g2 = 0;
				int _g1 = a.length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					_g.push(global::verb.core.Vec.dot(((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (a[i]) ))) ), b));
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<object> identity(int n) {
			global::HaxeArray<object> zeros = global::verb.core.Vec.zeros2d(n, n);
			{
				int _g1 = 0;
				int _g = n;
				while (( _g1 < _g )) {
					int i = _g1++;
					((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (zeros[i]) ))) )[i] = 1.0;
				}
				
			}
			
			return zeros;
		}
		
		
		public static global::HaxeArray<object> transpose<T>(global::HaxeArray<object> a) {
			if (( a.length == 0 )) {
				return new global::HaxeArray<object>(new object[]{});
			}
			
			global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
			{
				int _g2 = 0;
				int _g1 = ((global::HaxeArray<T>) (global::HaxeArray<object>.__hx_cast<T>(((global::Array) (a[0]) ))) ).length;
				while (( _g2 < _g1 )) {
					int i = _g2++;
					global::HaxeArray<T> _g3 = new global::HaxeArray<T>(new T[]{});
					{
						int _g5 = 0;
						int _g4 = a.length;
						while (( _g5 < _g4 )) {
							int j = _g5++;
							_g3.push(((global::HaxeArray<T>) (global::HaxeArray<object>.__hx_cast<T>(((global::Array) (a[j]) ))) )[i]);
						}
						
					}
					
					_g.push(_g3);
				}
				
			}
			
			return _g;
		}
		
		
		public static global::HaxeArray<double> solve(global::HaxeArray<object> A, global::HaxeArray<double> b) {
			return global::verb.core.Mat.LUsolve(global::verb.core.Mat.LU(A), b);
		}
		
		
		public static global::HaxeArray<double> LUsolve(global::verb.core._Mat.LUDecomp LUP, global::HaxeArray<double> b) {
			unchecked {
				int i = default(int);
				int j = default(int);
				global::HaxeArray<object> LU = LUP.LU;
				int n = LU.length;
				global::HaxeArray<double> x = b.copy();
				global::HaxeArray<int> P = LUP.P;
				int Pi = default(int);
				global::HaxeArray<double> LUi = null;
				object LUii = null;
				double tmp = default(double);
				i = ( n - 1 );
				while (( i != -1 )) {
					x[i] = b[i];
					 -- i;
				}
				
				i = 0;
				while (( i < n )) {
					Pi = P[i];
					if (( P[i] != i )) {
						tmp = x[i];
						x[i] = x[Pi];
						x[Pi] = tmp;
					}
					
					LUi = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (LU[i]) ))) );
					j = 0;
					while (( j < i )) {
						x[i] -= ( x[j] * LUi[j] );
						 ++ j;
					}
					
					 ++ i;
				}
				
				i = ( n - 1 );
				while (( i >= 0 )) {
					LUi = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (LU[i]) ))) );
					j = ( i + 1 );
					while (( j < n )) {
						x[i] -= ( x[j] * LUi[j] );
						 ++ j;
					}
					
					x[i] /= LUi[i];
					 -- i;
				}
				
				return x;
			}
		}
		
		
		public static global::verb.core._Mat.LUDecomp LU(global::HaxeArray<object> A) {
			unchecked {
				global::haxe.lang.Function abs = ((global::haxe.lang.Function) (new global::haxe.lang.Closure(typeof(global::HaxeMath), "abs", 4845682)) );
				int i = default(int);
				int j = default(int);
				int k = default(int);
				double absAjk = default(double);
				double Akk = default(double);
				global::HaxeArray<double> Ak = null;
				int Pk = default(int);
				global::HaxeArray<double> Ai = null;
				double max = default(double);
				global::HaxeArray<object> _g = new global::HaxeArray<object>(new object[]{});
				{
					int _g2 = 0;
					int _g1 = A.length;
					while (( _g2 < _g1 )) {
						int i1 = _g2++;
						_g.push(((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[i1]) ))) ).copy());
					}
					
				}
				
				A = _g;
				int n = A.length;
				int n1 = ( n - 1 );
				global::HaxeArray<int> P = new global::HaxeArray<int>();
				k = 0;
				while (( k < n )) {
					Pk = k;
					Ak = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[k]) ))) );
					max = global::System.Math.Abs(((double) (Ak[k]) ));
					j = ( k + 1 );
					while (( j < n )) {
						absAjk = global::System.Math.Abs(((double) (((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[j]) ))) )[k]) ));
						if (( max < absAjk )) {
							max = absAjk;
							Pk = j;
						}
						
						 ++ j;
					}
					
					P[k] = Pk;
					if (( Pk != k )) {
						A[k] = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[Pk]) ))) );
						A[Pk] = Ak;
						Ak = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[k]) ))) );
					}
					
					Akk = Ak[k];
					i = ( k + 1 );
					while (( i < n )) {
						((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[i]) ))) )[k] /= Akk;
						 ++ i;
					}
					
					i = ( k + 1 );
					while (( i < n )) {
						Ai = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (A[i]) ))) );
						j = ( k + 1 );
						while (( j < n1 )) {
							Ai[j] -= ( Ai[k] * Ak[j] );
							 ++ j;
							Ai[j] -= ( Ai[k] * Ak[j] );
							 ++ j;
						}
						
						if (( j == n1 )) {
							Ai[j] -= ( Ai[k] * Ak[j] );
						}
						
						 ++ i;
					}
					
					 ++ k;
				}
				
				return new global::verb.core._Mat.LUDecomp(A, P);
			}
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core._Mat {
	public class LUDecomp : global::haxe.lang.HxObject {
		
		public LUDecomp(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public LUDecomp(global::HaxeArray<object> lu, global::HaxeArray<int> p) {
			global::verb.core._Mat.LUDecomp.__hx_ctor_verb_core__Mat_LUDecomp(this, lu, p);
		}
		
		
		public static void __hx_ctor_verb_core__Mat_LUDecomp(global::verb.core._Mat.LUDecomp __hx_this, global::HaxeArray<object> lu, global::HaxeArray<int> p) {
			__hx_this.LU = lu;
			__hx_this.P = p;
		}
		
		
		public global::HaxeArray<object> LU;
		
		public global::HaxeArray<int> P;
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 80:
					{
						this.P = ((global::HaxeArray<int>) (global::HaxeArray<object>.__hx_cast<int>(((global::Array) (@value) ))) );
						return @value;
					}
					
					
					case 17033:
					{
						this.LU = ((global::HaxeArray<object>) (global::HaxeArray<object>.__hx_cast<object>(((global::Array) (@value) ))) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 80:
					{
						return this.P;
					}
					
					
					case 17033:
					{
						return this.LU;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("P");
			baseArr.push("LU");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


