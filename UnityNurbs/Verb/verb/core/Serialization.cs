// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public class SerializableBase : global::haxe.lang.HxObject {
		
		public SerializableBase(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public SerializableBase() {
			global::verb.core.SerializableBase.__hx_ctor_verb_core_SerializableBase(this);
		}
		
		
		public static void __hx_ctor_verb_core_SerializableBase(global::verb.core.SerializableBase __hx_this) {
		}
		
		
		public virtual string serialize() {
			global::haxe.Serializer serializer = new global::haxe.Serializer();
			serializer.serialize(this);
			return serializer.toString();
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1962040800:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "serialize", 1962040800)) );
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 1962040800:
					{
						return this.serialize();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public interface ISerializable : global::haxe.lang.IHxObject {
		
		string serialize();
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public class Deserializer : global::haxe.lang.HxObject {
		
		public Deserializer(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public Deserializer() {
			global::verb.core.Deserializer.__hx_ctor_verb_core_Deserializer(this);
		}
		
		
		public static void __hx_ctor_verb_core_Deserializer(global::verb.core.Deserializer __hx_this) {
		}
		
		
		public static T deserialize<T>(string s) {
			global::haxe.Unserializer unserializer = new global::haxe.Unserializer(((string) (s) ));
			T r = global::haxe.lang.Runtime.genericCast<T>(unserializer.unserialize());
			return r;
		}
		
		
	}
}


