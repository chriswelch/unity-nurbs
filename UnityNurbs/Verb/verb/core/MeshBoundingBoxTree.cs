// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public class MeshBoundingBoxTree : global::haxe.lang.HxObject, global::verb.eval.IBoundingBoxTree<int> {
		
		public MeshBoundingBoxTree(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public MeshBoundingBoxTree(global::verb.core.MeshData mesh, global::HaxeArray<int> faceIndices) {
			global::verb.core.MeshBoundingBoxTree.__hx_ctor_verb_core_MeshBoundingBoxTree(this, mesh, faceIndices);
		}
		
		
		public static void __hx_ctor_verb_core_MeshBoundingBoxTree(global::verb.core.MeshBoundingBoxTree __hx_this, global::verb.core.MeshData mesh, global::HaxeArray<int> faceIndices) {
			unchecked {
				__hx_this._empty = false;
				__hx_this._face = -1;
				if (( faceIndices == null )) {
					global::HaxeArray<int> _g = new global::HaxeArray<int>(new int[]{});
					{
						int _g2 = 0;
						int _g1 = mesh.faces.length;
						while (( _g2 < _g1 )) {
							int i = _g2++;
							_g.push(i);
						}
						
					}
					
					faceIndices = _g;
				}
				
				__hx_this._boundingBox = global::verb.core.Mesh.makeMeshAabb(mesh, faceIndices);
				if (( faceIndices.length < 1 )) {
					__hx_this._empty = true;
					return;
				}
				else if (( faceIndices.length < 2 )) {
					__hx_this._face = faceIndices[0];
					return;
				}
				
				global::HaxeArray<int> @as = global::verb.core.Mesh.sortTrianglesOnLongestAxis(__hx_this._boundingBox, mesh, faceIndices);
				global::HaxeArray<int> l = global::verb.core.ArrayExtensions.left<int>(((global::HaxeArray<int>) (@as) ));
				global::HaxeArray<int> r = global::verb.core.ArrayExtensions.right<int>(((global::HaxeArray<int>) (@as) ));
				__hx_this._children = new global::verb.core.Pair<object, object>(new global::verb.core.MeshBoundingBoxTree(mesh, l), new global::verb.core.MeshBoundingBoxTree(mesh, r));
			}
		}
		
		
		public virtual object verb_eval_IBoundingBoxTree_cast<T_c>() {
			return this;
		}
		
		
		public global::verb.core.Pair<object, object> _children;
		
		public global::verb.core.BoundingBox _boundingBox;
		
		public int _face;
		
		public bool _empty;
		
		public virtual global::verb.core.Pair<object, object> split() {
			return this._children;
		}
		
		
		public virtual global::verb.core.BoundingBox boundingBox() {
			return this._boundingBox;
		}
		
		
		public virtual int @yield() {
			return this._face;
		}
		
		
		public virtual bool indivisible(double tolerance) {
			return ( this._children == null );
		}
		
		
		public virtual bool empty() {
			return this._empty;
		}
		
		
		public override double __hx_setField_f(string field, int hash, double @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1992740988:
					{
						this._face = ((int) (@value) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField_f(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1807843790:
					{
						this._empty = global::haxe.lang.Runtime.toBool(@value);
						return @value;
					}
					
					
					case 1992740988:
					{
						this._face = ((int) (global::haxe.lang.Runtime.toInt(@value)) );
						return @value;
					}
					
					
					case 1806779144:
					{
						this._boundingBox = ((global::verb.core.BoundingBox) (@value) );
						return @value;
					}
					
					
					case 939528350:
					{
						this._children = ((global::verb.core.Pair<object, object>) (global::verb.core.Pair<object, object>.__hx_cast<object, object>(((global::verb.core.Pair) (@value) ))) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1876572813:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "empty", 1876572813)) );
					}
					
					
					case 2052836104:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "indivisible", 2052836104)) );
					}
					
					
					case 1899010637:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "yield", 1899010637)) );
					}
					
					
					case 94868743:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "boundingBox", 94868743)) );
					}
					
					
					case 24046298:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "split", 24046298)) );
					}
					
					
					case 1807843790:
					{
						return this._empty;
					}
					
					
					case 1992740988:
					{
						return this._face;
					}
					
					
					case 1806779144:
					{
						return this._boundingBox;
					}
					
					
					case 939528350:
					{
						return this._children;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override double __hx_getField_f(string field, int hash, bool throwErrors, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1992740988:
					{
						return ((double) (this._face) );
					}
					
					
					default:
					{
						return base.__hx_getField_f(field, hash, throwErrors, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 1876572813:
					{
						return this.empty();
					}
					
					
					case 2052836104:
					{
						return this.indivisible(((double) (global::haxe.lang.Runtime.toDouble(dynargs[0])) ));
					}
					
					
					case 1899010637:
					{
						return this.@yield();
					}
					
					
					case 94868743:
					{
						return this.boundingBox();
					}
					
					
					case 24046298:
					{
						return this.split();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("_empty");
			baseArr.push("_face");
			baseArr.push("_boundingBox");
			baseArr.push("_children");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


