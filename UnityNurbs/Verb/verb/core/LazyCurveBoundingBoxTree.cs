// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.core {
	public class LazyCurveBoundingBoxTree : global::haxe.lang.HxObject, global::verb.eval.IBoundingBoxTree<object> {
		
		public LazyCurveBoundingBoxTree(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public LazyCurveBoundingBoxTree(global::verb.core.NurbsCurveData curve, global::haxe.lang.Null<double> knotTol) {
			global::verb.core.LazyCurveBoundingBoxTree.__hx_ctor_verb_core_LazyCurveBoundingBoxTree(this, curve, knotTol);
		}
		
		
		public static void __hx_ctor_verb_core_LazyCurveBoundingBoxTree(global::verb.core.LazyCurveBoundingBoxTree __hx_this, global::verb.core.NurbsCurveData curve, global::haxe.lang.Null<double> knotTol) {
			unchecked {
				__hx_this._boundingBox = null;
				__hx_this._curve = curve;
				if ( ! (knotTol.hasValue) ) {
					global::HaxeArray<double> a = __hx_this._curve.knots;
					knotTol = new global::haxe.lang.Null<double>(( (( global::verb.core.ArrayExtensions.last<double>(((global::HaxeArray<double>) (a) )) - global::verb.core.ArrayExtensions.first<double>(((global::HaxeArray<double>) (a) )) )) / 64 ), true);
				}
				
				__hx_this._knotTol = (knotTol).@value;
			}
		}
		
		
		object global::verb.eval.IBoundingBoxTree<object>.yield() {
			return ((object) (this.@yield()) );
		}
		
		
		public virtual object verb_eval_IBoundingBoxTree_cast<T_c>() {
			return this;
		}
		
		
		public global::verb.core.NurbsCurveData _curve;
		
		public global::verb.core.BoundingBox _boundingBox;
		
		public double _knotTol;
		
		public virtual global::verb.core.Pair<object, object> split() {
			unchecked {
				double min = global::verb.core.ArrayExtensions.first<double>(((global::HaxeArray<double>) (this._curve.knots) ));
				double max = global::verb.core.ArrayExtensions.last<double>(((global::HaxeArray<double>) (this._curve.knots) ));
				double dom = ( max - min );
				global::HaxeArray<object> crvs = global::verb.eval.Divide.curveSplit(this._curve, ( ( (( max + min )) / 2.0 ) + ( ( dom * 0.1 ) * global::HaxeMath.rand.NextDouble() ) ));
				return new global::verb.core.Pair<object, object>(new global::verb.core.LazyCurveBoundingBoxTree(((global::verb.core.NurbsCurveData) (crvs[0]) ), new global::haxe.lang.Null<double>(this._knotTol, true)), new global::verb.core.LazyCurveBoundingBoxTree(((global::verb.core.NurbsCurveData) (crvs[1]) ), new global::haxe.lang.Null<double>(this._knotTol, true)));
			}
		}
		
		
		public virtual global::verb.core.BoundingBox boundingBox() {
			if (( this._boundingBox == null )) {
				this._boundingBox = new global::verb.core.BoundingBox(((global::HaxeArray<object>) (global::verb.eval.Eval.dehomogenize1d(this._curve.controlPoints)) ));
			}
			
			return this._boundingBox;
		}
		
		
		public virtual global::verb.core.NurbsCurveData @yield() {
			return this._curve;
		}
		
		
		public virtual bool indivisible(double tolerance) {
			global::HaxeArray<double> a = this._curve.knots;
			return ( ( global::verb.core.ArrayExtensions.last<double>(((global::HaxeArray<double>) (a) )) - global::verb.core.ArrayExtensions.first<double>(((global::HaxeArray<double>) (a) )) ) < this._knotTol );
		}
		
		
		public virtual bool empty() {
			return false;
		}
		
		
		public override double __hx_setField_f(string field, int hash, double @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1289351018:
					{
						this._knotTol = ((double) (@value) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField_f(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1289351018:
					{
						this._knotTol = ((double) (global::haxe.lang.Runtime.toDouble(@value)) );
						return @value;
					}
					
					
					case 1806779144:
					{
						this._boundingBox = ((global::verb.core.BoundingBox) (@value) );
						return @value;
					}
					
					
					case 1245680624:
					{
						this._curve = ((global::verb.core.NurbsCurveData) (@value) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1876572813:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "empty", 1876572813)) );
					}
					
					
					case 2052836104:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "indivisible", 2052836104)) );
					}
					
					
					case 1899010637:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "yield", 1899010637)) );
					}
					
					
					case 94868743:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "boundingBox", 94868743)) );
					}
					
					
					case 24046298:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "split", 24046298)) );
					}
					
					
					case 1289351018:
					{
						return this._knotTol;
					}
					
					
					case 1806779144:
					{
						return this._boundingBox;
					}
					
					
					case 1245680624:
					{
						return this._curve;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override double __hx_getField_f(string field, int hash, bool throwErrors, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1289351018:
					{
						return this._knotTol;
					}
					
					
					default:
					{
						return base.__hx_getField_f(field, hash, throwErrors, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 1876572813:
					{
						return this.empty();
					}
					
					
					case 2052836104:
					{
						return this.indivisible(((double) (global::haxe.lang.Runtime.toDouble(dynargs[0])) ));
					}
					
					
					case 1899010637:
					{
						return this.@yield();
					}
					
					
					case 94868743:
					{
						return this.boundingBox();
					}
					
					
					case 24046298:
					{
						return this.split();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("_knotTol");
			baseArr.push("_boundingBox");
			baseArr.push("_curve");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


