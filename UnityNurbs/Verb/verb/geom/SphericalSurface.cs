// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.geom {
	public class SphericalSurface : global::verb.geom.NurbsSurface {
		
		public SphericalSurface(global::haxe.lang.EmptyObject empty) : base(global::haxe.lang.EmptyObject.EMPTY) {
		}
		
		
		public SphericalSurface(global::HaxeArray<double> center, double radius) : base(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) )) {
			global::verb.geom.SphericalSurface.__hx_ctor_verb_geom_SphericalSurface(this, center, radius);
		}
		
		
		public static void __hx_ctor_verb_geom_SphericalSurface(global::verb.geom.SphericalSurface __hx_this, global::HaxeArray<double> center, double radius) {
			unchecked {
				global::verb.geom.NurbsSurface.__hx_ctor_verb_geom_NurbsSurface(__hx_this, global::verb.eval.Make.sphericalSurface(center, new global::HaxeArray<double>(new double[]{((double) (0) ), ((double) (0) ), ((double) (1) )}), new global::HaxeArray<double>(new double[]{((double) (1) ), ((double) (0) ), ((double) (0) )}), radius));
				__hx_this._center = center;
				__hx_this._radius = radius;
			}
		}
		
		
		public global::HaxeArray<double> _center;
		
		public double _radius;
		
		public virtual global::HaxeArray<double> center() {
			return this._center;
		}
		
		
		public virtual double radius() {
			return this._radius;
		}
		
		
		public override double __hx_setField_f(string field, int hash, double @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 527294961:
					{
						this._radius = ((double) (@value) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField_f(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 527294961:
					{
						this._radius = ((double) (global::haxe.lang.Runtime.toDouble(@value)) );
						return @value;
					}
					
					
					case 1951545204:
					{
						this._center = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (@value) ))) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 821481554:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "radius", 821481554)) );
					}
					
					
					case 98248149:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "center", 98248149)) );
					}
					
					
					case 527294961:
					{
						return this._radius;
					}
					
					
					case 1951545204:
					{
						return this._center;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override double __hx_getField_f(string field, int hash, bool throwErrors, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 527294961:
					{
						return this._radius;
					}
					
					
					default:
					{
						return base.__hx_getField_f(field, hash, throwErrors, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 821481554:
					{
						return this.radius();
					}
					
					
					case 98248149:
					{
						return this.center();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("_radius");
			baseArr.push("_center");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


