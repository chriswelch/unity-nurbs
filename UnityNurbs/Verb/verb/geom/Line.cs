// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.geom {
	public class Line : global::verb.geom.NurbsCurve {
		
		public Line(global::haxe.lang.EmptyObject empty) : base(global::haxe.lang.EmptyObject.EMPTY) {
		}
		
		
		public Line(global::HaxeArray<double> start, global::HaxeArray<double> end) : base(((global::haxe.lang.EmptyObject) (global::haxe.lang.EmptyObject.EMPTY) )) {
			global::verb.geom.Line.__hx_ctor_verb_geom_Line(this, start, end);
		}
		
		
		public static void __hx_ctor_verb_geom_Line(global::verb.geom.Line __hx_this, global::HaxeArray<double> start, global::HaxeArray<double> end) {
			global::verb.geom.NurbsCurve.__hx_ctor_verb_geom_NurbsCurve(__hx_this, global::verb.eval.Make.polyline(new global::HaxeArray<object>(new object[]{start, end})));
			__hx_this._start = start;
			__hx_this._end = end;
		}
		
		
		public global::HaxeArray<double> _start;
		
		public global::HaxeArray<double> _end;
		
		public virtual global::HaxeArray<double> start() {
			return this._start;
		}
		
		
		public virtual global::HaxeArray<double> end() {
			return this._end;
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 1058556124:
					{
						this._end = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (@value) ))) );
						return @value;
					}
					
					
					case 2146614179:
					{
						this._start = ((global::HaxeArray<double>) (global::HaxeArray<object>.__hx_cast<double>(((global::Array) (@value) ))) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 5047259:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "end", 5047259)) );
					}
					
					
					case 67859554:
					{
						return ((global::haxe.lang.Function) (new global::haxe.lang.Closure(this, "start", 67859554)) );
					}
					
					
					case 1058556124:
					{
						return this._end;
					}
					
					
					case 2146614179:
					{
						return this._start;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_invokeField(string field, int hash, global::Array dynargs) {
			unchecked {
				switch (hash) {
					case 5047259:
					{
						return this.end();
					}
					
					
					case 67859554:
					{
						return this.start();
					}
					
					
					default:
					{
						return base.__hx_invokeField(field, hash, dynargs);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("_end");
			baseArr.push("_start");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


