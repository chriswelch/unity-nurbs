// Generated by Haxe 3.4.7

#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.eval {
	public class Divide : global::haxe.lang.HxObject {
		
		public Divide(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public Divide() {
			global::verb.eval.Divide.__hx_ctor_verb_eval_Divide(this);
		}
		
		
		public static void __hx_ctor_verb_eval_Divide(global::verb.eval.Divide __hx_this) {
		}
		
		
		public static global::HaxeArray<object> surfaceSplit(global::verb.core.NurbsSurfaceData surface, double u, global::haxe.lang.Null<bool> useV) {
			unchecked {
				bool __temp_useV27 = ( ( ! (useV.hasValue) ) ? (false) : ((useV).@value) );
				global::HaxeArray<double> knots = null;
				int degree = default(int);
				global::HaxeArray<object> controlPoints = null;
				if ( ! (__temp_useV27) ) {
					controlPoints = global::verb.core.Mat.transpose<object>(((global::HaxeArray<object>) (surface.controlPoints) ));
					knots = surface.knotsU;
					degree = surface.degreeU;
				}
				else {
					controlPoints = surface.controlPoints;
					knots = surface.knotsV;
					degree = surface.degreeV;
				}
				
				global::HaxeArray<double> _g = new global::HaxeArray<double>(new double[]{});
				{
					int _g2 = 0;
					int _g1 = ( degree + 1 );
					while (( _g2 < _g1 )) {
						int i = _g2++;
						_g.push(u);
					}
					
				}
				
				global::HaxeArray<double> knots_to_insert = _g;
				global::HaxeArray<object> newpts0 = new global::HaxeArray<object>();
				global::HaxeArray<object> newpts1 = new global::HaxeArray<object>();
				int s = global::verb.eval.Eval.knotSpan(degree, u, knots);
				global::verb.core.NurbsCurveData res = null;
				{
					int _g11 = 0;
					while (( _g11 < controlPoints.length )) {
						global::HaxeArray<object> cps = ((global::HaxeArray<object>) (global::HaxeArray<object>.__hx_cast<object>(((global::Array) (controlPoints[_g11]) ))) );
						 ++ _g11;
						res = global::verb.eval.Modify.curveKnotRefine(new global::verb.core.NurbsCurveData(degree, knots, cps), knots_to_insert);
						newpts0.push(res.controlPoints.slice(0, new global::haxe.lang.Null<int>(( s + 1 ), true)));
						newpts1.push(res.controlPoints.slice(( s + 1 ), default(global::haxe.lang.Null<int>)));
					}
					
				}
				
				global::HaxeArray<double> knots0 = res.knots.slice(0, new global::haxe.lang.Null<int>(( ( s + degree ) + 2 ), true));
				global::HaxeArray<double> knots1 = res.knots.slice(( s + 1 ), default(global::haxe.lang.Null<int>));
				if ( ! (__temp_useV27) ) {
					newpts0 = global::verb.core.Mat.transpose<object>(((global::HaxeArray<object>) (newpts0) ));
					newpts1 = global::verb.core.Mat.transpose<object>(((global::HaxeArray<object>) (newpts1) ));
					return new global::HaxeArray<object>(new object[]{new global::verb.core.NurbsSurfaceData(degree, surface.degreeV, knots0, surface.knotsV.copy(), newpts0), new global::verb.core.NurbsSurfaceData(degree, surface.degreeV, knots1, surface.knotsV.copy(), newpts1)});
				}
				
				return new global::HaxeArray<object>(new object[]{new global::verb.core.NurbsSurfaceData(surface.degreeU, degree, surface.knotsU.copy(), knots0, newpts0), new global::verb.core.NurbsSurfaceData(surface.degreeU, degree, surface.knotsU.copy(), knots1, newpts1)});
			}
		}
		
		
		public static global::HaxeArray<object> curveSplit(global::verb.core.NurbsCurveData curve, double u) {
			unchecked {
				int degree = curve.degree;
				global::HaxeArray<object> controlPoints = curve.controlPoints;
				global::HaxeArray<double> knots = curve.knots;
				global::HaxeArray<double> _g = new global::HaxeArray<double>(new double[]{});
				{
					int _g2 = 0;
					int _g1 = ( degree + 1 );
					while (( _g2 < _g1 )) {
						int i = _g2++;
						_g.push(u);
					}
					
				}
				
				global::HaxeArray<double> knots_to_insert = _g;
				global::verb.core.NurbsCurveData res = global::verb.eval.Modify.curveKnotRefine(curve, knots_to_insert);
				int s = global::verb.eval.Eval.knotSpan(degree, u, knots);
				global::HaxeArray<double> knots0 = res.knots.slice(0, new global::haxe.lang.Null<int>(( ( s + degree ) + 2 ), true));
				global::HaxeArray<double> knots1 = res.knots.slice(( s + 1 ), default(global::haxe.lang.Null<int>));
				global::HaxeArray<object> cpts0 = res.controlPoints.slice(0, new global::haxe.lang.Null<int>(( s + 1 ), true));
				global::HaxeArray<object> cpts1 = res.controlPoints.slice(( s + 1 ), default(global::haxe.lang.Null<int>));
				return new global::HaxeArray<object>(new object[]{new global::verb.core.NurbsCurveData(degree, knots0, cpts0), new global::verb.core.NurbsCurveData(degree, knots1, cpts1)});
			}
		}
		
		
		public static global::HaxeArray<object> rationalCurveByEqualArcLength(global::verb.core.NurbsCurveData curve, int num) {
			double tlen = global::verb.eval.Analyze.rationalCurveArcLength(curve, default(global::haxe.lang.Null<double>), default(global::haxe.lang.Null<int>));
			double inc = ( tlen / num );
			return global::verb.eval.Divide.rationalCurveByArcLength(curve, inc);
		}
		
		
		public static global::HaxeArray<object> rationalCurveByArcLength(global::verb.core.NurbsCurveData curve, double l) {
			global::HaxeArray<object> crvs = global::verb.eval.Modify.decomposeCurveIntoBeziers(curve);
			global::HaxeArray<double> crvlens = crvs.map<double>(((global::haxe.lang.Function) (( (( global::verb.eval.Divide_rationalCurveByArcLength_145__Fun.__hx_current != null )) ? (global::verb.eval.Divide_rationalCurveByArcLength_145__Fun.__hx_current) : (global::verb.eval.Divide_rationalCurveByArcLength_145__Fun.__hx_current = ((global::verb.eval.Divide_rationalCurveByArcLength_145__Fun) (new global::verb.eval.Divide_rationalCurveByArcLength_145__Fun()) )) )) ));
			double totlen = global::verb.core.Vec.sum(crvlens);
			global::HaxeArray<object> pts = new global::HaxeArray<object>(new object[]{new global::verb.eval.CurveLengthSample(curve.knots[0], 0.0)});
			if (( l > totlen )) {
				return pts;
			}
			
			double inc = l;
			int i = 0;
			double lc = inc;
			double runsum = 0.0;
			double runsum1 = 0.0;
			double u = default(double);
			while (( i < crvs.length )) {
				runsum += crvlens[i];
				while (( lc < ( runsum + global::verb.core.Constants.EPSILON ) )) {
					u = global::verb.eval.Analyze.rationalBezierCurveParamAtArcLength(((global::verb.core.NurbsCurveData) (crvs[i]) ), ( lc - runsum1 ), new global::haxe.lang.Null<double>(global::verb.core.Constants.TOLERANCE, true), new global::haxe.lang.Null<double>(crvlens[i], true));
					pts.push(new global::verb.eval.CurveLengthSample(u, lc));
					lc += inc;
				}
				
				runsum1 += crvlens[i];
				 ++ i;
			}
			
			return pts;
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.eval {
	public class Divide_rationalCurveByArcLength_145__Fun : global::haxe.lang.Function {
		
		public Divide_rationalCurveByArcLength_145__Fun() : base(1, 1) {
		}
		
		
		public static global::verb.eval.Divide_rationalCurveByArcLength_145__Fun __hx_current;
		
		public override double __hx_invoke1_f(double __fn_float1, object __fn_dyn1) {
			global::verb.core.NurbsCurveData x = ( (( __fn_dyn1 == global::haxe.lang.Runtime.undefined )) ? (((global::verb.core.NurbsCurveData) (((object) (__fn_float1) )) )) : (((global::verb.core.NurbsCurveData) (__fn_dyn1) )) );
			return global::verb.eval.Analyze.rationalBezierCurveArcLength(x, default(global::haxe.lang.Null<double>), default(global::haxe.lang.Null<int>));
		}
		
		
	}
}



#pragma warning disable 109, 114, 219, 429, 168, 162
namespace verb.eval {
	public class CurveLengthSample : global::haxe.lang.HxObject {
		
		public CurveLengthSample(global::haxe.lang.EmptyObject empty) {
		}
		
		
		public CurveLengthSample(double u, double len) {
			global::verb.eval.CurveLengthSample.__hx_ctor_verb_eval_CurveLengthSample(this, u, len);
		}
		
		
		public static void __hx_ctor_verb_eval_CurveLengthSample(global::verb.eval.CurveLengthSample __hx_this, double u, double len) {
			__hx_this.u = u;
			__hx_this.len = len;
		}
		
		
		public double u;
		
		public double len;
		
		public override double __hx_setField_f(string field, int hash, double @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 5393365:
					{
						this.len = ((double) (@value) );
						return @value;
					}
					
					
					case 117:
					{
						this.u = ((double) (@value) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField_f(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_setField(string field, int hash, object @value, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 5393365:
					{
						this.len = ((double) (global::haxe.lang.Runtime.toDouble(@value)) );
						return @value;
					}
					
					
					case 117:
					{
						this.u = ((double) (global::haxe.lang.Runtime.toDouble(@value)) );
						return @value;
					}
					
					
					default:
					{
						return base.__hx_setField(field, hash, @value, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override object __hx_getField(string field, int hash, bool throwErrors, bool isCheck, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 5393365:
					{
						return this.len;
					}
					
					
					case 117:
					{
						return this.u;
					}
					
					
					default:
					{
						return base.__hx_getField(field, hash, throwErrors, isCheck, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override double __hx_getField_f(string field, int hash, bool throwErrors, bool handleProperties) {
			unchecked {
				switch (hash) {
					case 5393365:
					{
						return this.len;
					}
					
					
					case 117:
					{
						return this.u;
					}
					
					
					default:
					{
						return base.__hx_getField_f(field, hash, throwErrors, handleProperties);
					}
					
				}
				
			}
		}
		
		
		public override void __hx_getFields(global::HaxeArray<object> baseArr) {
			baseArr.push("len");
			baseArr.push("u");
			base.__hx_getFields(baseArr);
		}
		
		
	}
}


